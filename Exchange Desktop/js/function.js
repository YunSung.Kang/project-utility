$(function () {

    // add 'is-last-child' class
    $(document).ready(function () {
        $(".input-group .valid-feedback").prev().addClass("is-last-child");
        $(".input-group .invalid-feedback").prev().addClass("is-last-child");
    });

    // slide Off canvas action
    $(function () {
        // slide Off canvas add active action
        function slideOffCanvas_open() {
            var winScrollTop = $(window).scrollTop();
            $('.soc-wrap').addClass('active');
            $('.full-veil').fadeIn();
            $('.body').addClass('noScroll');
            $('body').scrollTop(winScrollTop);
            // console.log(scrollTop);
        }
        $('.btnSocOpen').on('click', function () {
            slideOffCanvas_open();
        });
        // slide Off canvas remove active action
        function slideOffCanvas_close() {
            var bodyScrollTop = $('body').scrollTop();
            $('.soc-wrap').removeClass('active');
            $('.full-veil').fadeOut();
            $('.body').removeClass('noScroll');
            $('body').scrollTop(0);
            $(window).scrollTop(bodyScrollTop);
            // console.log(scrollTop);
        }
        $('.btnSocClose').on('click', function () {
            slideOffCanvas_close();
        });
        $('.full-veil').on('click', function () {
            slideOffCanvas_close();
        });
        // slide Off canvas remove active action
        $('.soc-body .soc-sub-title').on('click', function () {
            $(this).toggleClass('active');
            $(this).next().slideToggle('fast');
        });
        $(window).resize(function () {
            if ($(window).width() > 991.99) {
                slideOffCanvas_close();
            }
        });
    });

});