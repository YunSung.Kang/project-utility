$(function () {

    // common variable
    var winWidth = $(window).width();
    var winHeight = $(window).height();
    var contentAreaHeight = $('#contentAreaHeight').height();



    $('.btn-minify').on('click', function () {
        $('#wrapper-body').toggleClass('miniNav');
        $('#wrapper-body').toggleClass('normalNav');
    });


    // main side navigation add class name
    $('.main-side-navigation').addClass('main-nav');
    $('.main-side-navigation>li>ul').addClass('sub-nav');

    $('.main-side-navigation>li>a').on('click', function () {
        if ($(this).parent('li').is('.active')) {
            $(this).next('.sub-nav').removeClass('active');
            $(this).parent('li').removeClass('active');
        } else {
            $('li.active .sub-nav').removeClass('active');
            $('li.active').removeClass('active');
            $(this).next('.sub-nav').addClass('active');
            $(this).parent('li').addClass('active');
        }
    });
    $('.sub-nav>li>a').on('click', function () {
        if ($(this).parent('li').is('.active')) {
            $(this).parent('li').removeClass('active');
        } else {
            $('.sub-nav>li.active').removeClass('active');
            $(this).parent('li').addClass('active');
        }
    });

    $(document).ready(function () {

        // 모바일용 좌우 메뉴 작동
        $('#mobileTopNavL').append(
            '<div class="state1"></div>\n' +
            '<div class="state2"></div>'
        );
        $('#mobileTopNavR').append(
            '<div class="state1"></div>\n' +
            '<div class="state2"></div>'
        );
        function mobileTopNavL_open() {
            $('#mobileTopNavL').addClass('active');
            $('.nav-aside-left').addClass('active');
            $('.full-veil').fadeIn();
        }
        function mobileTopNavL_close() {
            $('#mobileTopNavL').removeClass('active');
            $('.nav-aside-left').removeClass('active');
            $('.full-veil').fadeOut();
        }
        function mobileTopNavR_open() {
            $('#mobileTopNavR').addClass('active');
            $('.nav-aside-right').addClass('active');
            $('.full-veil').fadeIn();
        }
        function mobileTopNavR_close() {
            $('#mobileTopNavR').removeClass('active');
            $('.nav-aside-right').removeClass('active');
            $('.full-veil').fadeOut();
        }
        function mobileBody_open() {
            var winScrollTop = $(window).scrollTop();
            $('.body').addClass('noScroll');
            $('body').scrollTop(winScrollTop);
            // console.log(winScrollTop);
        }
        function mobileBody_close() {
            var bodyScrollTop = $('body').scrollTop();
            $('.body').removeClass('noScroll');
            $('body').scrollTop(0);
            $(window).scrollTop(bodyScrollTop);
            // console.log(scrollTop);
        }
        $('#mobileTopNavL>.state1').on('click', function () {
            mobileTopNavL_open();
            mobileBody_open();
        })
        $('#mobileTopNavL>.state2').on('click', function () {
            mobileTopNavL_close();
            mobileBody_close();
        })
        $('#mobileTopNavR>.state1').on('click', function () {
            mobileTopNavR_open();
            mobileBody_open();
        })
        $('#mobileTopNavR>.state2').on('click', function () {
            mobileTopNavR_close();
            mobileBody_close();
        })
        $('.full-veil').on('click', function () {
            mobileTopNavL_close();
            mobileTopNavR_close();
            mobileBody_close();
        })









        $(window).resize(function () {


            // toggle side navigation action
            var winWidth = $(window).width();
            if (winWidth > 991.99) {
                mobileTopNavL_close();
                mobileTopNavR_close();
                mobileBody_close();
            } else {
                $('#wrapper-body').removeClass('miniNav');
                $('#wrapper-body').addClass('normalNav');
            }

        });

    });

});