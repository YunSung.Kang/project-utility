$(function () {

    // common variable

    var winWidth = $(window).width();
    var winHeight = $(window).height();
    var navPadding = 112; // nav and footer height
    var contentAreaHeight = $('#contentAreaHeight').height();


    // footer position
    // if not enghout contents, footer on the bottom

    function footer_PositionBottom() {
        if (winHeight > contentAreaHeight + navPadding) {
            $('.footer').addClass('fixed');
        }
    }



    // layer popup position
    // layer popup on the center of browser

    function layerpopup_PositionCenter() {
        var layerpopupHeight = $('.lp-container').height();
        var layerpopupMarginTop = -(layerpopupHeight / 2) - 24;
        $('.lp-container').css('margin-top', layerpopupMarginTop);
    }



    // slider

    function slider_ImgPostion() {
        var sliderHeight = $(".slider-item").height();
        var sliderImgHeight = $(".slider-item img").height();
        if (sliderHeight < sliderImgHeight) {
            var sliderMargin = -(sliderImgHeight - sliderHeight) / 2
            $(".slider-item img").css("margin-top", sliderMargin)
            // console.log(sliderMargin);
        }
    }



    // aside navigation active action

    function asideNavigation_open() {
        var winScrollTop = $(window).scrollTop();
        $('.btn-top-toggle').addClass('active');
        $('.top-nav-wrap').addClass('active');
        $('.full-veil').fadeIn();
        $('.body').addClass('noScroll');
        $('body').scrollTop(winScrollTop);
        // console.log(scrollTop);
    }



    // aside navigation disactive action

    function asideNavigation_close() {
        var bodyScrollTop = $('body').scrollTop();
        $('.btn-top-toggle').removeClass('active');
        $('.top-nav-wrap').removeClass('active');
        $('.full-veil').fadeOut();
        $('.body').removeClass('noScroll');
        $('body').scrollTop(0);
        $(window).scrollTop(bodyScrollTop);
        // console.log(scrollTop);
    }



    // header hidden action with scroll

    var delta = 15;
    var lastScrollTop = 0;

    $(document).scroll(function (event) {
        var thisScrollTop = $(this).scrollTop();
        if (Math.abs(lastScrollTop - thisScrollTop) <= delta) return;
        if ((thisScrollTop > lastScrollTop) && (lastScrollTop > 0)) {
            $('.header').addClass('hidden');
        } else {
            $('.header').removeClass('hidden');
        }
        lastScrollTop = thisScrollTop;
    });



    // footer position

    footer_PositionBottom();

    // layer popup position

    layerpopup_PositionCenter();



    $(document).ready(function () {

        // add 'is-last-child' class

        $(".input-group .valid-feedback").prev().addClass("is-last-child");
        $(".input-group .invalid-feedback").prev().addClass("is-last-child");



        // loading header

        var docScrollValue = $(document).scrollTop();
        if (docScrollValue > navPadding) {
            $('.header').addClass('hidden');
        }



        // toggle side navigation action

        $('.btn-top-toggle .fa-bars').on('click', function () {
            asideNavigation_open();
        })

        $('.btn-top-toggle .fa-times').on('click', function () {
            asideNavigation_close();
        })

        $('.full-veil').on('click', function () {
            asideNavigation_close();
        })



        // datepicker

        $('.datepicker').datepicker();



        // slider

        slider_ImgPostion();

    });



    $(window).resize(function () {

        // toggle side navigation action

        var winWidth = $(window).width();
        if (winWidth > 991.99) {
            asideNavigation_close();
        }



        // slider

        slider_ImgPostion();

    });

});